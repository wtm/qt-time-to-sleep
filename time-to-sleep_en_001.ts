<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en_001">
<context>
    <name>NotificationWindow</name>
    <message>
        <location filename="notificationwindow.ui" line="23"/>
        <location filename="build/time-to-sleep_autogen/include/ui_notificationwindow.h" line="196"/>
        <source>NotificationWindow</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="notificationwindow.ui" line="57"/>
        <location filename="build/time-to-sleep_autogen/include/ui_notificationwindow.h" line="197"/>
        <source>It&apos;s time to sleep!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="notificationwindow.ui" line="157"/>
        <location filename="build/time-to-sleep_autogen/include/ui_notificationwindow.h" line="199"/>
        <source>systemctl suspend</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="notificationwindow.ui" line="160"/>
        <location filename="build/time-to-sleep_autogen/include/ui_notificationwindow.h" line="201"/>
        <source>Suspend</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="notificationwindow.ui" line="178"/>
        <location filename="build/time-to-sleep_autogen/include/ui_notificationwindow.h" line="202"/>
        <source>Snooze</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="notificationwindow.ui" line="226"/>
        <location filename="build/time-to-sleep_autogen/include/ui_notificationwindow.h" line="203"/>
        <source>lock screen and suspend</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="notificationwindow.ui" line="257"/>
        <location filename="build/time-to-sleep_autogen/include/ui_notificationwindow.h" line="204"/>
        <source>for 5 minutes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="notificationwindow.ui" line="296"/>
        <location filename="build/time-to-sleep_autogen/include/ui_notificationwindow.h" line="205"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; color:#ed333b;&quot;&gt;?&lt;/span&gt; snoozes left&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message numerus="yes">
        <location filename="notificationwindow.cpp" line="46"/>
        <source>&lt;b&gt;%n&lt;/b&gt; snooze(s) left</source>
        <translation type="unfinished">
            <numerusform></numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
</context>
<context>
    <name>SnoozeTimerWindow</name>
    <message>
        <location filename="snoozetimerwindow.ui" line="17"/>
        <location filename="build/time-to-sleep_autogen/include/ui_snoozetimerwindow.h" line="65"/>
        <source>MainWindow</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="snoozetimerwindow.ui" line="50"/>
        <location filename="build/time-to-sleep_autogen/include/ui_snoozetimerwindow.h" line="66"/>
        <source>00:00</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
