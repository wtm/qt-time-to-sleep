#include "oneinstancelock.h"
#include <QDebug>
#include <QDir>
#include <QLockFile>
#include <QStandardPaths>
#include <QtGlobal>
#include <memory>
#include <stdexcept>

std::unique_ptr<QLockFile> getExclusionLock() {
  auto run_dir =
      QStandardPaths::writableLocation(QStandardPaths::RuntimeLocation);
  QDir d(run_dir);
  if (!d.mkpath(".")) {
    throw std::runtime_error("Could not create run directory.");
  }
  auto lock_path = d.absoluteFilePath("time-to-sleep.lock");
  auto lf = std::make_unique<QLockFile>(lock_path);
  if (lf->tryLock(0)) {
    return lf;
  } else {
    qWarning() << "One-instance exclusion lock exists: " << lock_path;
    return std::unique_ptr<QLockFile>(nullptr);
  }
}
