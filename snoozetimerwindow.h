#ifndef SNOOZETIMERWINDOW_H
#define SNOOZETIMERWINDOW_H

#include <QMainWindow>
#include <chrono>

class AppState;

namespace Ui {
class SnoozeTimerWindow;
}

class SnoozeTimerWindow : public QMainWindow {
  Q_OBJECT

public:
  explicit SnoozeTimerWindow(AppState &state, QWidget *parent = nullptr);
  ~SnoozeTimerWindow();

  void updateView(AppState &);
  bool isClosed();

protected:
  void closeEvent(QCloseEvent *) override;

private slots:
  void timerTick();

private:
  Ui::SnoozeTimerWindow *ui = nullptr;
  std::chrono::time_point<std::chrono::steady_clock> snooze_end_time;
  bool closed = false;
};

#endif // SNOOZETIMERWINDOW_H
