#pragma once

#include <QLockFile>
#include <memory>

std::unique_ptr<QLockFile> getExclusionLock();
