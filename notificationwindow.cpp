#include "notificationwindow.h"
#include "./ui_notificationwindow.h"
#include "appstate.h"

#include <QCloseEvent>
#include <QFuture>
#include <QProcess>
#include <QShortcut>
#include <QTime>
#include <QTimer>
#include <QtConcurrent>
#include <QtDebug>
#include <QtGlobal>

NotificationWindow::NotificationWindow(const AppState &initial_state,
                                       QWidget *parent)
    : QMainWindow(parent), ui(new Ui::NotificationWindow),
      timer(new QTimer(this)), closed(false) {
  ui->setupUi(this);

  this->setWindowFlag(Qt::WindowStaysOnTopHint, true);
  this->updateView(initial_state);

  auto quit_shortcut = new QShortcut(QKeySequence(QKeySequence::Quit), this);
  connect(quit_shortcut, &QShortcut::activated, this,
          &NotificationWindow::doClose);

  connect(timer, &QTimer::timeout, this, &NotificationWindow::doUpdate);
  timer->setInterval(1000);
  timer->start();

  this->show();
}

NotificationWindow::~NotificationWindow() {
  if (this->suspend_future) {
    this->suspend_future->waitForFinished();
  }

  delete ui;
}

bool NotificationWindow::isClosed() { return this->closed; }

void NotificationWindow::updateView(const AppState &state) {
  ui->snoozes_left_label->setText(
      tr("<b>%n</b> snooze(s) left", "", state.snoozes_left)
          .replace("<b>", "<span style=\"color: #ed333b;\">")
          .replace("</b>", "</span>"));
  ui->snooze->setDisabled(state.snoozes_left == 0);
  this->doUpdate();
}

void NotificationWindow::doClose() {
  if (this->closed) {
    return;
  }
  this->closed = true;
  this->doCleanup();
  this->close();
}

void NotificationWindow::ensureFront() {
  if (closed) {
    return;
  }
  this->showFullScreen();
  this->activateWindow();
}

void NotificationWindow::changeEvent(QEvent *evt) {
  if (this->closed) {
    return;
  }
  if (evt->type() == QEvent::ActivationChange && !this->isActiveWindow()) {
    this->ensureFront();
  }
}

void NotificationWindow::closeEvent(QCloseEvent *evt) {
  if (!this->closed) {
    evt->ignore();
  }
}

void NotificationWindow::doCleanup() { timer->stop(); }

void NotificationWindow::doUpdate() {
  auto now_hhmm = QTime::currentTime().toString("HH:mm");
  ui->clock_label->setText(now_hhmm);
  this->ui->suspend->setEnabled(!this->suspend_future.has_value() ||
                                this->suspend_future->isFinished());
  this->ensureFront();
}

void NotificationWindow::on_snooze_clicked() { emit onSnooze(); }

void NotificationWindow::on_suspend_clicked() {
  emit onSuspend();

  auto fut = QtConcurrent::run([]() {
#if defined(Q_OS_LINUX)
    if (QProcess::execute("loginctl", QStringList{"lock-session"}) != 0) {
      return false;
    }
    if (QProcess::execute("systemctl", QStringList{"suspend"}) != 0) {
      return false;
    }
#elif defined(Q_OS_WIN)
    if (QProcess::execute("rundll32.exe", QStringList{"powrprof.dll,SetSuspendState", "0,1,0"}) != 0) {
      return false;
    }
#else
#endif
    QThread::msleep(2000);
    return true;
  });

  fut.then(this, [this](bool success) { this->doUpdate(); });

  this->suspend_future = std::optional(std::move(fut));
  this->doUpdate();
}
