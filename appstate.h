#pragma once

#include "notificationwindow.h"
#include "snoozetimerwindow.h"

#include <QTime>
#include <chrono>
#include <memory>
#include <optional>

const int SNOOZES_TOTAL = 3;
const std::chrono::duration SNOOZE_TIME = std::chrono::minutes(5);
const QTime MORNING_CUTOFF(5, 0);
const QTime SLEEP_TIME(22, 0);

struct AppState {
  int snoozes_left;
  QTime sleep_time;
  std::unique_ptr<NotificationWindow> current_notification_win;
  std::unique_ptr<SnoozeTimerWindow> snooze_timer_window;
  std::optional<std::chrono::time_point<std::chrono::steady_clock>>
      current_snooze_end;

  AppState();
};
