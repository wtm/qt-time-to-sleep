#include <QApplication>
#include <QLocale>
#include <QTime>
#include <QTranslator>
#include <memory>

#include "appstate.h"
#include "notificationwindow.h"
#include "oneinstancelock.h"

void initTranslate(QApplication &);
void doUpdate(AppState &, QTimer *);

int main(int argc, char *argv[]) {
  QApplication a(argc, argv);

  auto lock = getExclusionLock();
  if (!lock) {
    return 1;
  }

  a.connect(&a, &QApplication::aboutToQuit,
            [lock = std::move(lock)]() { lock->unlock(); });

  a.setQuitOnLastWindowClosed(false);
  initTranslate(a);

  AppState state;

  auto update_timer = new QTimer(&a);
  doUpdate(state, update_timer);
  a.connect(update_timer, &QTimer::timeout,
            [&state, update_timer]() { doUpdate(state, update_timer); });
  update_timer->start();

  return a.exec();
}

void initTranslate(QApplication &a) {
  QTranslator translator;
  const QStringList uiLanguages = QLocale::system().uiLanguages();
  for (const QString &locale : uiLanguages) {
    const QString baseName = "time-to-sleep_" + QLocale(locale).name();
    if (translator.load(":/i18n/" + baseName)) {
      a.installTranslator(&translator);
      break;
    }
  }
}

void doUpdate(AppState &state, QTimer *timer) {
  QTime now = QTime::currentTime();

  bool should_sleep = now > state.sleep_time || now < MORNING_CUTOFF;
  if (should_sleep) {
    timer->setInterval(1000);
    if (state.current_snooze_end.has_value() &&
        *state.current_snooze_end < std::chrono::steady_clock::now()) {
      state.current_snooze_end.reset();
    }
    if (!state.current_snooze_end.has_value() &&
        (!state.current_notification_win ||
         state.current_notification_win->isClosed())) {
      if (state.snooze_timer_window) {
        state.snooze_timer_window->close();
        state.snooze_timer_window = nullptr;
      }
      state.current_notification_win =
          std::make_unique<NotificationWindow>(state);
      auto win = state.current_notification_win.get();
      qApp->connect(win, &NotificationWindow::onSnooze, [win, &state]() {
        if (state.snoozes_left == 0) {
          return;
        }
        state.snoozes_left -= 1;
        state.current_snooze_end =
            std::chrono::steady_clock::now() + SNOOZE_TIME;
        win->doClose();
        state.current_notification_win = nullptr;

        state.snooze_timer_window = std::make_unique<SnoozeTimerWindow>(state);
      });
    }
  } else {
    timer->setInterval(20000);
    if (state.current_notification_win) {
      state.current_notification_win->doClose();
      state.current_notification_win.reset();
    }
    if (state.snooze_timer_window) {
      state.snooze_timer_window->close();
      state.snooze_timer_window = nullptr;
    }
    state.snoozes_left = SNOOZES_TOTAL;
    state.current_snooze_end.reset();
  }
}
