#pragma once

#include <QCloseEvent>
#include <QEvent>
#include <QMainWindow>
#include <QTimer>
#include <QFuture>
#include <optional>

QT_BEGIN_NAMESPACE
namespace Ui {
class NotificationWindow;
}
QT_END_NAMESPACE

class AppState;

class NotificationWindow : public QMainWindow {
  Q_OBJECT

public:
  NotificationWindow(const AppState &initial_state, QWidget *parent = nullptr);
  ~NotificationWindow();
  void updateView(const AppState &);
  bool isClosed();

signals:
  void onSnooze();
  void onSuspend();

protected:
  void changeEvent(QEvent *) override;
  void closeEvent(QCloseEvent *) override;

public slots:
  void doClose();

private slots:
  void doCleanup();
  void doUpdate();

  void on_snooze_clicked();

  void on_suspend_clicked();

private:
  Ui::NotificationWindow *ui = nullptr;
  QTimer *timer = nullptr;
  bool closed = false;
  std::optional<QFuture<bool>> suspend_future;

  void ensureFront();
};
