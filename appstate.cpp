#include "appstate.h"

AppState::AppState()
    : snoozes_left(SNOOZES_TOTAL), sleep_time(SLEEP_TIME),
      current_notification_win(nullptr), snooze_timer_window(nullptr),
      current_snooze_end() {}
