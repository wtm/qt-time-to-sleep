#include <QScreen>
#include <QTimer>
#include <chrono>

#include "appstate.h"
#include "snoozetimerwindow.h"
#include "ui_snoozetimerwindow.h"

SnoozeTimerWindow::SnoozeTimerWindow(AppState &state, QWidget *parent)
    : QMainWindow(parent), ui(new Ui::SnoozeTimerWindow) {
  ui->setupUi(this);
  this->setWindowFlag(Qt::WindowStaysOnTopHint, true);
  this->updateView(state);

  auto timer = new QTimer(this);
  timer->setInterval(500);
  timer->start();
  connect(timer, &QTimer::timeout, this, &SnoozeTimerWindow::timerTick);

  this->setWindowFlag(Qt::CoverWindow, true);
  this->setWindowFlag(Qt::Popup, true);
  this->setWindowFlag(Qt::FramelessWindowHint, true);
  this->setWindowFlag(Qt::NoDropShadowWindowHint, true);
  this->setWindowFlag(Qt::WindowStaysOnTopHint, true);
  this->setWindowFlag(Qt::WindowDoesNotAcceptFocus, true);
  this->setWindowFlag(Qt::BypassWindowManagerHint, true);

  this->show();

  auto sg = this->screen()->geometry();
  this->move(sg.bottomRight() - this->geometry().bottomRight());
}

SnoozeTimerWindow::~SnoozeTimerWindow() { delete ui; }

bool SnoozeTimerWindow::isClosed() { return this->closed; }

void SnoozeTimerWindow::updateView(AppState &state) {
  this->snooze_end_time =
      state.current_snooze_end.value_or(std::chrono::steady_clock::now());
  this->timerTick();
}

void SnoozeTimerWindow::closeEvent(QCloseEvent *evt) { this->closed = true; }

void SnoozeTimerWindow::timerTick() {
  if (this->closed) {
    return;
  }
  auto now = std::chrono::steady_clock::now();
  auto d = this->snooze_end_time - now;
  if (this->snooze_end_time < now) {
    d = d.zero();
  }
  int seconds = std::chrono::duration_cast<std::chrono::seconds>(d).count();
  int minutes = seconds / 60;
  seconds %= 60;
  this->ui->timer_label->setText(QString("%1:%2")
                                     .arg(minutes, 2, 10, QChar('0'))
                                     .arg(seconds, 2, 10, QChar('0')));
}
